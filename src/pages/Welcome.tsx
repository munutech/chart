import React, { useEffect } from 'react'
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Card, Typography, Alert } from 'antd';
import { HorizontalBar } from 'react-chartjs-2';
// 引入 ECharts 主模块(这里路径引入错误参考上文文档描述)
import echarts from 'echarts'
import vis from 'vis'
import styles from './Welcome.less';

// 引入柱状图（这里放你需要使用的echarts类型 很重要）
import  'echarts/lib/chart/bar';
import 'echarts/lib/chart/pie'
import 'echarts/lib/component/title'

const CodePreview: React.FC<{}> = ({ children }) => (
  <pre className={styles.pre}>
    <code>
      <Typography.Text copyable>{children}</Typography.Text>
    </code>
  </pre>
);

const ChartTable = () => {
  const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'My First dataset',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data: [65, 59, 80, 81, 56, 55, 40],
      },
    ],
  };


  return (
    <div >
      <HorizontalBar data={data} width={600} height={250} />
    </div>
  );

};

const Cat = () => {

  useEffect(() => {
    const script = document.createElement("script");

    script.src = "https://prod-apnortheast-a.online.tableau.com/javascripts/api/viz_v1.js";
    script.async = true;
  
    document.body.appendChild(script);
  })
  // const url = 'https://visjs.github.io/vis-timeline/examples/graph2d/03_groups.html'
  // const url = 'https://prod-apnortheast-a.online.tableau.com/t/songzewen/views/Test1/sheet0?:showAppBanner=false&:display_count=n&:showVizHome=n&:origin=viz_share_link'
  // const scrolling = 'auto'
  const html1 = `<div class='tableauPlaceholder' style='width: 1000px; height: 670px;overflow: auto'>
  <object class='tableauViz' width='1000' height='670' style='display:none;overflow: auto'>
  <param name='host_url' value='https%3A%2F%2Fprod-apnortheast-a.online.tableau.com%2F' />
   <param name='embed_code_version' value='3' /> <param name='site_root' value='&#47;t&#47;songzewen' />
   <param name='name' value='Test1&#47;sheet0' /><param name='tabs' value='yes' />
   <param name='toolbar' value='yes' /><param name='showAppBanner' value='false' /><
   /object></div>`
  return (
    <div>
      {/* <div className='tableauPlaceholder' style={{width: '1440px', height: '670px'}}>
        <object className='tableauViz'  width='1440' height='670' style={{display:'none'}}>
        <param name='host_url' value='https%3A%2F%2Fprod-apnortheast-a.online.tableau.com%2F' /> <param name='embed_code_version' value='3' />
        <param name='site_root' value='&#47;t&#47;songzewen' /><param name='name' value='Test1&#47;sheet0' /><param name='tabs' value='yes' />
        <param name='toolbar' value='yes' /><param name='showAppBanner' value='false' />
         </object>
      </div> */}
      <div dangerouslySetInnerHTML={{__html:html1}}></div>
      {/* <iframe title='chartIframe' style={{width:'1000px',height:'600px'}} id="mainIframe" name='mainIframe' src={url} frameBorder='0'  scrolling={scrolling} /> */}
    </div>
  );

};

const VisTest = () => {

  useEffect(() => {
  // create an array with nodes
  const NODES = new vis.DataSet([
    {id: 1, label: 'Node 1'},
    {id: 2, label: 'Node 2'},
    {id: 3, label: 'Node 3'},
    {id: 4, label: 'Node 4'},
    {id: 5, label: 'Node 5'}
  ]);

  // create an array with edges
  const EDGES = new vis.DataSet([
    {from: 1, to: 3, id:'a'},
    {from: 1, to: 2},
    {from: 2, to: 4},
    {from: 2, to: 5},
    {from: 3, to: 3}
  ]);

  // create a network
  const container = document.getElementById('mynetwork');

  const data = {
    nodes: NODES,
    edges: EDGES
  };
  let options = {};
  options = {
    layout: {
        randomSeed: 1,
    },
    height: '100%',
    physics: {
        enabled: false,
    },
    nodes: {//node的设置
        size: 80,
        font: {
            size: 22,
            color: '#999',
        },
        borderWidth: 1,
    },
    edges: {//edge的设置
        arrows: {
            to: { enabled: false, scaleFactor: 0 }, // 箭头大小
            middle: { enabled: false, scaleFactor: 0 },
            from: { enabled: false, scaleFactor: 0 },
        },
        chosen: {
            edge(values:any) {
                values.color = 'blue'
            },

        },
        color: {
            // color: "gray",
            highlight: 'blue',
        },
        font: {
            size: 20,
            // color: 'yellow',
            align: 'top',
        },
    },
    interaction: {
        dragNodes: true,
        dragView: true, // 可拖动
        zoomView: true,
        selectConnectedEdges: true,
        navigationButtons: true, // 控制导航
        keyboard: true, // 快捷键控制导航
    },
    groups: {
      "ASW.10GE": {//这个组用哪个图片显示   
        "image": "./img/ASW.10GE.png",
        "shape": "image"
      },
    },
    }
  new vis.Network(container as HTMLDivElement, data, options);
  })
  return (
    <div>
      <div style={{width: "500px", height: "500px"}}  id='mynetwork' />
    </div>
  )  
};
const EchartsTest = () => {
  function initChart () {
    const element = document.getElementById('network');
    const myChart = echarts.init(element as HTMLDivElement);
    const option = {
      title: {
          text: 'Graph 简单示例'
      },
      tooltip: {},
      animationDurationUpdate: 1500,
      animationEasingUpdate: 'quinticInOut',
      series: [
          {
              type: 'graph',
              layout: 'none',
              symbolSize: 50,
              roam: true,
              label: {
                  show: true
              },
              edgeSymbol: ['circle', 'arrow'],
              edgeSymbolSize: [4, 10],
              edgeLabel: {
                  fontSize: 20
              },
              data: [{
                  name: '节点1',
                  x: 0,
                  y: 0
              }, {
                  name: '节点2',
                  x: 800,
                  y: 300
              }, {
                  name: '节点3',
                  x: 550,
                  y: 100
              }, {
                  name: '节点4',
                  x: 550,
                  y: 500
              }],
              // links: [], mqtt
              links: [{
                  source: 0,
                  target: 1,
                  symbolSize: [5, 20],
                  label: {
                      show: true
                  },
                  lineStyle: {
                      width: 5,
                      curveness: 0.2
                  }
              }, {
                  source: '节点2',
                  target: '节点1',
                  label: {
                      show: true
                  },
                  lineStyle: {
                      curveness: 0.2
                  }
              }, {
                  source: '节点1',
                  target: '节点3'
              }, {
                  source: '节点2',
                  target: '节点3'
              }, {
                  source: '节点2',
                  target: '节点4'
              }, {
                  source: '节点1',
                  target: '节点4'
              }],
              lineStyle: {
                  opacity: 0.9,
                  width: 2,
                  curveness: 0
              }
          }
      ]
  };
    myChart.setOption(option);
  }
  useEffect(() => {
    initChart()
    console.log(11)
  })
  return (
    <div>
      <div id='network' style={{width: "500px", height: "500px"}}  />
    </div>
  )  
}
export default (): React.ReactNode => {
  function aa () {
    const element = document.getElementById('main');
    const myChart = echarts.init(element as HTMLDivElement);
    const option = {
    title: {
        text: 'ECharts 入门示例',
    },
    tooltip: {
    },
    legend: {
        data:['销量', '利润', '比率']
    },
    xAxis: {
        data: ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"]
    },
    yAxis: {
    },
    series: [
        {
            name: '销量',
            type: 'bar',
            data: [5, 20, 36, 10, 10, 20]
        },
        {
            name: '利润',
            type: 'bar',
            data: [30, 25, 15, 20, 20, 35]
        },
        {
            name: '比率',
            type: 'line',
            data: [35, 30, 20, 25, 25, 40]
        }]
    };
    myChart.setOption(option);
  }
  useEffect(() => {
    aa()
    console.log(11)
  })
  // function initViz() {
  //   var containerDiv = document.getElementById("vizContainer"),
  //   url = "https://YOUR-SERVER/views/YOUR-VISUALIZATION";

  //   new tableau.Viz(containerDiv, url);
  // }
  // useEffect(() => {
  //   initViz()
  // })
  return (
  <PageHeaderWrapper>
    <Card>
      <div id="vizContainer"></div>
      <Alert
        message="umi ui 现已发布，点击右下角 umi 图标即可使用"
        type="success"
        showIcon
        banner
        style={{
          margin: -12,
          marginBottom: 24,
        }}
      />
      {/* <VisTest /> */}
      <ChartTable></ChartTable>
      <Typography.Text strong>
        <a target="_blank" rel="noopener noreferrer" href="https://pro.ant.design/docs/block">
          基于 block 开发，快速构建标准页面
        </a>
      </Typography.Text>
        <Cat />
        {/* <Chart></Chart> */}
        <EchartsTest />
        <div id='main' style={{width: "500px", height: "500px"}}  />
        <div>------------------------------------------</div>
        <h3>vis范例</h3>
        <VisTest />
      <CodePreview> npm run ui</CodePreview>
      <Typography.Text
        strong
        style={{
          marginBottom: 12
        }}
      >
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://pro.ant.design/docs/available-script#npm-run-fetchblocks"
        >
          获取全部区块
        </a>
      </Typography.Text>
      <CodePreview> npm run fetch:blocks</CodePreview>
    </Card>
    <p
      style={{
        textAlign: 'center',
        marginTop: 24,
      }}
    >
      Want to add more pages? Please refer to{' '}
      <a href="https://pro.ant.design/docs/block-cn" target="_blank" rel="noopener noreferrer">
        use block
      </a>
      。
    </p>
  </PageHeaderWrapper>
  )
    };
